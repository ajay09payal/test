/**
 * This javascript file will constitute the entry point of your solution.
 *
 * Edit it as you need.  It currently contains things that you might find helpful to get started.
 */

// This is not really required, but means that changes to index.html will cause a reload.
require('./site/index.html')
// Apply the styles in style.css to the page.
require('./site/style.css')

// Using Lodash library for sorting of data
require('lodash');

// if you want to use es6, you can do something like
//     require('./es6/myEs6code')
// here to load the myEs6code.js file, and it will be automatically transpiled.

// Change this to get detailed logging from the stomp library
global.DEBUG = false

const url = "ws://localhost:8011/stomp"
const client = Stomp.client(url)
client.debug = function (msg) {
  if (global.DEBUG) {
    console.info(msg)
  }
}

function connectCallback() {
  try {
    const subscription = client.subscribe('/fx/prices', currencyDataCallback, {});
  } catch (error) {
    alert('Conenction unsuccesful ' + error);
  }
}

client.connect({}, connectCallback, function error(error) {
  alert(error.headers.message)
});

// array used to append value to table cess
let arrColumns = ['name', 'bestBid', 'bestAsk', 'openBid', 'openAsk', 'lastChangeBid', 'lastChangeAsk', 'sparklineGraph'];

// sparkline currency graph
let tableData = {
  arrSparklineCell: [],
  arrSparklineCellData: [],
  arrTableCurrencyData: []
};
/**
 * Desc : Function used for callback
 * @parram = objCurrency 
 * 
 */
function currencyDataCallback(objCurrency) {
  try {
    const objCurrencyData = JSON.parse(objCurrency.body);
    let bolUpdatedRecord = false;
    let previousIndex = '';

    const tableId = document.getElementById("myTable");

    //  check for duplicate values
    tableData.arrTableCurrencyData = tableData.arrTableCurrencyData.filter((objCurrency, intIndex) => {
      if (objCurrency.name === objCurrencyData.name) {
        previousIndex = intIndex;
        bolUpdatedRecord = true;
      }
      return tableData.arrTableCurrencyData[intIndex];
    });

    // calculate mid price
    let intMidPrice = (parseFloat(objCurrencyData.bestBid) + parseFloat(objCurrencyData.bestAsk)) / 2;

    // condition used to check update / add row
    if (!bolUpdatedRecord) {

      // push new receive value into array
      tableData.arrTableCurrencyData.push(objCurrencyData);

      // replace old value into array for creating sparkline
      tableData.arrSparklineCellData['sparklineGraph' + objCurrencyData.name] = [intMidPrice];

      //  add new table row
      createRow(tableId, objCurrencyData, 'add', tableData.arrTableCurrencyData);
    } else if (previousIndex !== '') {

      //  remove the midPrice after 30 seconds for sparkline graph
      setTimeout(() => {
        tableData.arrSparklineCellData['sparklineGraph' + objCurrencyData.name].shift();
      }, 30000);

      // update old value of array with new value
      tableData.arrTableCurrencyData[previousIndex] = objCurrencyData;

      // push new value into array for creating sparkline
      tableData.arrSparklineCellData['sparklineGraph' + objCurrencyData.name].push(intMidPrice);

      // update table row with new value
      createRow(tableId, objCurrencyData, 'delete', tableData.arrTableCurrencyData);
    }
  } catch (err) {
    alert('Conenction unsuccesful ' + error);
  }
}


/**
 * Desc : function used to create table and sparkline
 * @parram = tableId = Table id
 * @parram = objCurrencyData = data reveice from API
 * @parram = bolType = type need to check for add / update row 
 * @parram = arrTableCurrencyData  = currecny data receive from API 
 * 
 */
function createRow(tableId, objCurrencyData, bolType, arrTableCurrencyData) {

  try {
    // Sort currency data
    arrTableCurrencyData = _.orderBy(arrTableCurrencyData, ['lastChangeBid'], ['desc']);

    // find table row index used to add / update row 
    const currentIndex = arrTableCurrencyData.findIndex(currencyPair => currencyPair.name === objCurrencyData.name);

    // Delete row
    if (bolType === 'delete') {
      deleteRow(tableId, currentIndex)
    }

    // add new row
    addRow(tableId, objCurrencyData, currentIndex);
  } catch (error) {
    alert('Conenction unsuccesful ' + error);
  }

}

/**
 * Desc : delete old row
 * 
 */
function deleteRow(tableId, currentIndex) {
  try {
    tableId.deleteRow(currentIndex + 1);
  } catch (error) {
    alert('Conenction unsuccesful ' + error);
  }
}

/**
 * Desc : add new row
 * 
 */
function addRow(tableId, objCurrencyData, currentIndex) {
  try {
    // used to check where system need to insert row
    let row = tableId.insertRow(currentIndex + 1);

    // insert new cell data
    let cell;

    for (let i = 0; i < arrColumns.length; i++) {
      cell = row.insertCell(i);
      //  check for last column of sparkline graph
      if (arrColumns[i] === 'sparklineGraph') {
        let cellRefName = 'sparklineGraph' + objCurrencyData.name;

        //  create a sparkline element reference object
        tableData.arrSparklineCell[cellRefName] = new Sparkline(cell, {
          width: 200
        });

        //raw a sparkline graph
        tableData.arrSparklineCell[cellRefName].draw(tableData.arrSparklineCellData[cellRefName]);
      } else {
        cell.innerHTML = objCurrencyData[arrColumns[i]];
      }
    }
  } catch (error) {
    alert('Conenction unsuccesful ' + error);
  }
}